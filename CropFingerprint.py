import os
import cv2
import time
import urllib.request


def crop_fingerprint(image_name, image_path, output_folder):
    # width = 2550
    # height = 3510
    img_in = cv2.imread(image_path, 0)
    height, width = img_in.shape
    list_cropped_path = []
    # Create subfolder in output_folder as image name:
    output_path = os.path.join(output_folder, image_name.split(".")[0])
    if not os.path.isdir(output_path):
        os.makedirs(output_path)
    # Right hands:
    print(round(0.38*height))
    right_thumb = img_in[round(0.38*height):round(0.52*height), round(0.04*width):round(0.23*width)]
    cv2.imwrite(output_path+"/right_thumb.png", right_thumb)
    list_cropped_path.append(image_name.split(".")[0] + "/right_thumb.png")
    right_index_finger = img_in [round(0.38*height):round(0.52*height), round(0.225*width):round(0.41*width)]
    cv2.imwrite(output_path + "/right_index_finger.png", right_index_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/right_index_finger.png")
    right_middle_finger = img_in [round(0.38*height):round(0.52*height), round(0.40*width):round(0.585*width)]
    cv2.imwrite(output_path + "/right_middle_finger.png", right_middle_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/right_middle_finger.png")
    right_ring_finger = img_in [round(0.38*height):round(0.52*height), round(0.57*width):round(0.76*width)]
    cv2.imwrite(output_path + "/right_ring_finger.png", right_ring_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/right_ring_finger.png")
    right_little_finger = img_in[round(0.38*height):round(0.52*height), round(0.765*width):round(0.93*width)]
    cv2.imwrite(output_path + "/right_little_finger.png", right_little_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/right_little_finger.png")
    right_extra_thumb = img_in[round(0.68*height):round(0.83*height), round(0.495*width):round(0.623*width)]
    cv2.imwrite(output_path + "/right_extra_thumb.png", right_extra_thumb)
    list_cropped_path.append(image_name.split(".")[0] + "/right_extra_thumb.png")
    four_right_fingers = img_in[round(0.66*height):round(0.85*height), round(0.62*width):round(0.94*width)]
    cv2.imwrite(output_path + "/four_right_fingers.png", four_right_fingers)
    list_cropped_path.append(image_name.split(".")[0] + "/four_right_fingers.png")

    # Left hands:
    left_thumb = img_in[round(0.52*height):round(0.66*height), round(0.05*width):round(0.25*width)]
    cv2.imwrite(output_path + "/left_thumb.png", left_thumb)
    list_cropped_path.append(image_name.split(".")[0] + "/left_thumb.png")
    left_index_finger = img_in[round(0.52*height):round(0.66*height), round(0.23*width):round(0.42*width)]
    cv2.imwrite(output_path + "/left_index_finger.png", left_index_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/left_index_finger.png")
    left_middle_finger = img_in[round(0.52*height):round(0.66*height), round(0.41*width):round(0.59*width)]
    cv2.imwrite(output_path + "/left_middle_finger.png", left_middle_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/left_middle_finger.png")
    left_ring_finger = img_in[round(0.52*height):round(0.66*height), round(0.58*width):round(0.77*width)]
    cv2.imwrite(output_path + "/left_ring_finger.png", left_ring_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/left_ring_finger.png")
    left_little_finger = img_in[round(0.52*height):round(0.66*height), round(0.76*width):round(0.95*width)]
    cv2.imwrite(output_path + "/left_little_finger.png", left_little_finger)
    list_cropped_path.append(image_name.split(".")[0] + "/left_little_finger.png")
    left_extra_thumb = img_in[round(0.68*height):round(0.83*height), round(0.365*width):round(0.50*width)]
    cv2.imwrite(output_path + "/left_extra_thumb.png", left_extra_thumb)
    list_cropped_path.append(image_name.split(".")[0] + "/left_extra_thumb.png")
    four_left_fingers = img_in[round(0.66*height):round(0.85*height), round(0.05*width):round(0.38*width)]
    cv2.imwrite(output_path + "/four_left_fingers.png", four_left_fingers)
    list_cropped_path.append(image_name.split(".")[0] + "/four_left_fingers.png")
    print("Done !!!")
    print(list_cropped_path)
    return list_cropped_path


if __name__ == '__main__':
    # crop_fingerprint('test3.jpeg', 'images/test3.jpeg', 'cropped')
    # crop_fingerprint('test_standard.jpeg', 'images/test_standard.jpeg', 'cropped')


