import os
from flask import Flask, url_for, send_file, escape, request, render_template, redirect, jsonify, make_response, \
    session, send_from_directory
from flask_cors import CORS
import time
import urllib.request
from CropFingerprint import crop_fingerprint

app = Flask(__name__)
CORS(app)

UPLOAD_FOLDER = 'image_uploaded'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

CROPPED_FOLDER = 'cropped'
app.config['CROPPED_FOLDER'] = CROPPED_FOLDER

URL = '127.0.0.1'
port = 5000


@app.route('/crop_fingerprint', methods=['GET', 'POST'])
def predict():
    # Get image from url and save to folder: image_uploaded with new image name as time uploaded
    # Example: host:port/crop_fingerprint?url=image_url, with image url is the url address of image
    new_image_name = str(time.time()).replace(".","_") + '.png'
    image_path = os.path.join(app.config['UPLOAD_FOLDER'], new_image_name)
    urllib.request.urlretrieve(request.args.get('url'), image_path)

    # Put new image into crop function, return a list in order: right - thumb, index finger, middle finger, ring finger, little finger, extra thumb, 4 fingers ; left - ...
    list_cropped = crop_fingerprint(new_image_name, image_path, app.config['CROPPED_FOLDER'])
    return jsonify({'status':1, 'list_cropped': list_cropped})

if __name__ == '__main__':
    app.run(debug=True, host=URL, port=port)


